import csv
import time
import itertools


def total_data():
    with open('school_data.csv') as csv_file:

        # Part 0 i.e Reading CSV file using delimiter=',' as default
        # I could have went with DictReader as well but emphasis has to be done on Computation Speed
        csv_reader = csv.reader(csv_file)
        next(csv_reader)  # Skipping Header

        # Storing Data in List as a part of PreProcessing
        rows = [row for row in csv_reader]
        return rows


def total_schools(rows):
    # 1.1
    print(f'Total Schools : {len(rows)}')


def school_by_states(rows):
    # 1.2
    # Went for groupBy feature of itertools
    print('Schools by State:')
    school_by_states = itertools.groupby(rows, key=lambda x: x[5])
    for state, school_count in school_by_states:
        print(f'{state} : {len(list(school_count))}')


def school_by_metro_centric(rows):
    # 1.3
    print('Schools by Metro-centric locale: ')
    #  GroupBy is less Efficient to calculate number of Schools per Metro Centric
    #  because it works best when we have to group consecutive data
    #  Hence went with the below approach
    school_by_metro_centric = {}
    for row in rows:
        school_by_metro_centric[row[8]] = school_by_metro_centric.get(row[8], 0) + 1

    for metro_centric_code, school_count in school_by_metro_centric.items():
        print(f'{metro_centric_code}:{school_count}')
    # Data has discrepancy as 'N' cant be a Metro Centric Locale Code
    # it has to be an integer as per the documentation


def city_with_most_schools(rows):
    # 1.4
    school_by_cities = {}
    for row in rows:
        school_by_cities[row[4]] = school_by_cities.get(row[4], 0) + 1

    city_with_max_schools = max(school_by_cities.keys(), key=school_by_cities.get)
    # Another Approach city_with_max_schools = max(school_by_cities.keys(), key=lambda x: x[0])
    print(f'City with most schools: '
          f'{city_with_max_schools.upper()} ({school_by_cities.get(city_with_max_schools)} schools) ')

    return school_by_cities


def total_cities_with_one_school(rows):
    # 1.5
    school_by_cities = {}
    for row in rows:
        school_by_cities[row[4]] = school_by_cities.get(row[4], 0) + 1

    cities_counter_with_one_school = itertools.count()
    for city, school_count in school_by_cities.items():
        if school_count == 1:
            next(cities_counter_with_one_school)
    print(f'Unique cities with at least one school: {next(cities_counter_with_one_school)}')


def print_counts():
    rows = total_data()
    # Data PreProcessed
    total_schools(rows)
    print()

    start_time = time.time()
    school_by_states(rows)
    print(f'Tt Took {int((time.time() - start_time) * 1000)} milliSec to fetch All Schools By States')
    print()

    start_time = time.time()
    school_by_metro_centric(rows)
    print(f'Tt Took {int((time.time() - start_time) * 1000)} milliSec to fetch Schools By Metro Centric Locale')
    print()

    start_time = time.time()
    city_with_most_schools(rows)
    print(f'Tt Took {int((time.time() - start_time) * 1000)} milliSec to fetch City with Most Schools')
    print()

    start_time = time.time()
    total_cities_with_one_school(rows)
    print(f'Tt Took {int((time.time() - start_time) * 1000)} milliSec to fetch Total Cities with one School')

    # Below appraoch of using only one function is clumsy way of coding
    # hence went for using multiple functions for every requirement
    # ReUsability , Readability and Maintainability of Code hs been taken into consideration
    # with open('school_data.csv') as csv_file:
    #     start_time = time.time()
    #
    #     # Part 0 i.e Reading CSV file using delimiter=',' as default
    #     # I could have went with DictReader as well but emphasis has to be done on Computation Speed
    #     csv_reader = csv.reader(csv_file)
    #     next(csv_reader)  # Skipping Header
    #
    #     # Storing Data in List as a part of PreProcessing
    #     rows = [row for row in csv_reader]
    #
    #     # Part 1 i.e Computational Statistics
    #     # 1.1
    #     print(f'Total Schools : {len(rows)}')
    #
    #     # 1.2
    #     # Went for groupBy feature of itertools
    #     print('Schools by State:')
    #     school_by_states = itertools.groupby(rows, key=lambda x: x[5])
    #     for state, school_count in school_by_states:
    #         print(f'{state} : {len(list(school_count))}')
    #
    #     # 1.3
    #     print('Schools by Metro-centric locale: ')
    #     #  GroupBy is less Efficient to calculate number of Schools per Metro Centric
    #     #  because it works best when we have to group consecutive data
    #     #  Hence went with the below approach
    #     schools_by_metro_centric = {}
    #     for row in rows:
    #         schools_by_metro_centric[row[8]] = schools_by_metro_centric.get(row[8], 0) + 1
    #
    #     for metro_Centric_locale_code, school_count in schools_by_metro_centric.items():
    #         print(f'{metro_Centric_locale_code}:{school_count}')
    #     # Data has discrepancy as 'N' cant be a Metro Centric Locale Code
    #     # it has to be an integer as per the documentation
    #
    #     # 1.4
    #     school_by_cities = {}
    #     for row in rows:
    #         school_by_cities[row[4]] = school_by_cities.get(row[4], 0) + 1
    #
    #     city_with_max_schools = max(school_by_cities.keys(), key=school_by_cities.get)
    #     # Another Approach city_with_max_schools = max(school_by_cities.keys(), key=lambda x: x[0])
    #     print(f'City with most schools: {city_with_max_schools.upper()} ({school_by_cities.get(city_with_max_schools)} schools) ')
    #
    #     # 1.5
    #     cities_counter_with_one_school = itertools.count()
    #     for city, school_count in school_by_cities.items():
    #         if school_count == 1:
    #             next(cities_counter_with_one_school)
    #     print(f'Unique cities with at least one school: {next(cities_counter_with_one_school)}')
    #
    #     print(f'Time Took {int((time.time() - start_time) * 1000)} milliSec')


if __name__ == '__main__':
    print_counts()

