import csv
import itertools
import time


def total_data():
    with open('school_data.csv') as csv_file:
        # Reading CSV file using delimiter=',' as default
        # I could have went with DictReader as well but emphasis has to be done on Computation Speed
        csv_reader = csv.reader(csv_file)
        next(csv_reader)  # Skipping Header

        # Storing Data in List as a part of PreProcessing
        rows = [row for row in csv_reader]
        return rows


# Ignore below approach it was happy path
# def search_schools(query):
#     query = query.upper()
#     rows = total_data()
#     required_database = [row[3:6] for row in rows]
#
#     keywords = query.split()
#     # No need to search for keyword school of given query
#     if 'SCHOOL' in keywords:
#         keywords.remove('SCHOOL')
#
#     weightage_dict = {}
#
#     # Data PreProcessed, Hence we can start our timer to calculate computation speed of the query
#     start_time = time.time()
#     row_counter = itertools.count()
#     for school_name, city, state in required_database:
#         total_matches_counter = itertools.count()
#
#         for keyword in keywords:
#             if keyword in school_name:
#                 next(total_matches_counter)
#
#             if keyword in city:
#                 next(total_matches_counter)
#
#             if keyword in state:
#                 next(total_matches_counter)
#
#         weightage_dict[next(row_counter)] = next(total_matches_counter)
#
#     for row_index, weightage in sorted(weightage_dict.items(), key=lambda x: x[1], reverse=True)[:3]:
#         if weightage is not None:
#             print(required_database[row_index])
#
#     print(f'Tt Took {int((time.time() - start_time) * 1000)} milliSec to fetch the Query')

def search_schools(query):
    query = query.upper()
    rows = total_data()
    required_database = [row[3:6] for row in rows]

    keywords = query.split()
    list_weightage_dict = []

    # Data PreProcessed, Hence we can start our timer to calculate computation speed of the query
    start_time = time.time()
    row_counter = itertools.count()
    for school_name, city, state in required_database:
        total_matches_counter = itertools.count()

        weightage_dict = {}
        for keyword in keywords:
            if keyword in school_name:
                weightage_dict['school_name_weightage'] = weightage_dict.get('school_name_weightage', 0) + 1
                next(total_matches_counter)

            if keyword in city:
                weightage_dict['city_weightage'] = weightage_dict.get('city_weightage', 0) + 1
                next(total_matches_counter)

            if keyword in state:
                weightage_dict['state_weightage'] = weightage_dict.get('state_weightage', 0) + 1
                next(total_matches_counter)

        weightage_dict['row_index'] = next(row_counter)
        weightage_dict['total_weightage'] = next(total_matches_counter)

        #  Append weightage Dict if only there is atleast single match i.e total matches counter is greater than 0
        #  If Given String doesnt match any of School Name, City or State then its definitely not a valid record
        if weightage_dict['total_weightage'] > 0:
            list_weightage_dict.append(weightage_dict)

    sorted_list_weightage = sorted(list_weightage_dict, key=lambda i: i['total_weightage'], reverse=True)

    # Query Optimisation goes here
    # Not Implementing more Query Optimisation because there are many scenarios to handel and due to time constraint
    #  I am not implementing it for time being
    # if sorted_list_weightage[0].get('total_weightage') == 1:
    #     for relevant_data in sorted_list_weightage:
    #         if relevant_data['school_name_weightage'] == 1:
    #             print(required_database[int(relevant_data['row_index'])])
    #
    # total_printed_result = 0
    # if sorted_list_weightage[0].get('total_weightage') == 2:
    #     #      We have got count of two searches that can be both in school name or one in school name and other in any of state or city
    #     #  In such case we will try to output result which has searches in school name and city or state
    #     for relevant_data in sorted_list_weightage:
    #         if not relevant_data['school_name_weightage'] == 2:
    #             total_printed_result = total_printed_result + 1
    #             if total_printed_result < 4:
    #                 print(required_database[int(relevant_data['row_index'])])
    #             else:
    #                 break

    #  Extended Algorithm to be implemented in Future
    #  I could go with percent of query string matching with school name, city name or state name
    #  if either of them matches 100% then it would return that result
    #  but that would be too long process and due to time constraint i am not implementing for time being
    # Further we have to take decision which one to prioritise matching more percentage of query with either school name or city or state
    #  or trying to have match with not just school name but trying to cover as much as of school name and city or state

    # Print first 3 relevant Searches if atleast there is a single match
    print(f'Results for {query} (search took: {int((time.time() - start_time) * 1000)} milliSec)')
    for index, relevant_data in enumerate(sorted_list_weightage[:3], start=1):
        if relevant_data['total_weightage'] > 0:
            required_data = required_database[int(relevant_data['row_index'])]
            print(f'{index}. {required_data[0]} \n'
                  f'{required_data[1]}, {required_data[2]}')


if __name__ == '__main__':
    #  Note :- I didnt Understood whats [Next Best Hit] hence didnt implemented it
    # Different Scenarios run
    search_schools('elementary school highland park')
    print()

    search_schools('jefferson belleville')
    print()

    search_schools('riverside school 44')
    print()

    search_schools('granada charter school')
    print()

    search_schools('foley high alabama')
    print()

    search_schools('KUSKOKWIM')
    # Check out the Outputs, Every Query takes less than 200 ms for sure and some time as less as 50 ms ,
    # try running the same query for at least 5 times to look for the best execution time of all
